import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  StatusBar,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import axios from 'axios';

export default class DeleteChecklist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.route.params.token,
    };
  }

  componentDidMount() {
    console.log('INI ADALAH TOKEN : ', this.props.route.params.token);
    //REQUEST GET ALL CHECKLIST
    axios
      .get(`http://94.74.86.174:8080/api/checklist`, {
        headers: {Authorization: `Bearer ${this.state.token}`},
      })
      .then(response => {
        console.log('INI ADALAH RESPON GET ALL CHECKLIST : ', response.data);
        if (
          response.data.message === 'Proses view all berhasil' &&
          response.data.statusCode === 2100
        ) {
          this.setState({
            data: response.data.data,
          });
        } else {
          console.log('GAGAL REQUEST');
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  //INI ADALAH FUNGSI HAPUS CHECKLIST
  deleteChecklist(id) {
    axios
      .delete(`http://94.74.86.174:8080/api/checklist/${id}`, {
        headers: {Authorization: `Bearer ${this.state.token}`},
      })
      .then(response => {
        console.log('INI ADALAH RESPON DELETE CHECKLIST : ', response.data);
        if (
          response.data.message === 'Proses delete berhasil' &&
          response.data.statusCode === 2300
        ) {
          alert("Proses delete berhasil")
        } else {
          console.log('GAGAL REQUEST');
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={{flex: 1}}
          keyExtractor={(item, index) => index.toString()}
          data={this.state.data}
          renderItem={({item, index}) => (
            // <Text style={{fontSize: 100}}>1</Text>
            <TouchableOpacity
                onPress={() => this.deleteChecklist(item.id)}
              style={
                index % 2 == 0 ? styles.highlighttrue : styles.highlightfalse
              }>
              <Text style={{color: 'white', fontWeight: 'bold'}}>
                {'ID : ' + item.id + '  Name : ' + item.name}
              </Text>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // height: 200
    // width: '100%'
    // marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  highlightfalse: {
    backgroundColor: 'grey',
    height: 100,
    justifyContent: 'center',
  },
  highlighttrue: {
    backgroundColor: 'black',
    height: 100,
    justifyContent: 'center',
  },
});
