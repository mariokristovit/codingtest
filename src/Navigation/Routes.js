import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  Homescreen,
  DropdownProvince,
  DummyApi,
  OpenCamera,
  Login,
  Register,
  GetAllChecklist,
  CreateNewChecklist,
  DeleteChecklist,
  GetAllItem
} from '../Screens';
const Stack = createNativeStackNavigator();

function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Homescreen" component={Homescreen} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="GetAllChecklist" component={GetAllChecklist} />
        <Stack.Screen name="CreateNewChecklist" component={CreateNewChecklist} />
        <Stack.Screen name="DeleteChecklist" component={DeleteChecklist} />
        <Stack.Screen name="GetAllItem" component={GetAllItem} />
        <Stack.Screen
          name="DropdownProvince"
          component={DropdownProvince}
          options={{title: 'Dropdown Fiture'}}
        />
        <Stack.Screen
          name="DummyApi"
          component={DummyApi}
          options={{title: 'Dummy API Fiture'}}
        />
        <Stack.Screen
          name="OpenCamera"
          component={OpenCamera}
          options={{title: 'Open Camera Fiture'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routes;
